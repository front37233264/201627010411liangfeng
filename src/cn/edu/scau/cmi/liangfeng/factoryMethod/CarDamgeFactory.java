package cn.edu.scau.cmi.liangfeng.factoryMethod;

import cn.edu.scau.cmi.liangfeng.domain.CarDamage;
import cn.edu.scau.cmi.liangfeng.domain.Insurance;

public class CarDamgeFactory implements IFactory {

	private static CarDamgeFactory instance;
	
	public static CarDamgeFactory getFactory(){
		if (instance == null){
			instance = new CarDamgeFactory();
		}
		return instance;
	}
	
	@Override
	public Insurance CreateInsurance() {
		
		return new CarDamage();
	}

}
