package cn.edu.scau.cmi.liangfeng.factoryMethod;

import cn.edu.scau.cmi.liangfeng.domain.Insurance;

public interface IFactory {
	Insurance CreateInsurance();
}
