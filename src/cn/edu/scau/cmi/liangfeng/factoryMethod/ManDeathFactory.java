package cn.edu.scau.cmi.liangfeng.factoryMethod;

import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.domain.ManDeath;

public class ManDeathFactory implements IFactory {

	private static ManDeathFactory instance;
	
	public static ManDeathFactory getFactory(){
		if (instance == null){
			instance = new ManDeathFactory();
		}
		return instance;
	}
	@Override
	public Insurance CreateInsurance() {
		
		return new ManDeath();
	}

}
