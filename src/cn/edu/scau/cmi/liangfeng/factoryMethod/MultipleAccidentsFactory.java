package cn.edu.scau.cmi.liangfeng.factoryMethod;

import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.domain.MultipleAccidents;

public class MultipleAccidentsFactory implements IFactory {

	private static MultipleAccidentsFactory instance;
	
	public static MultipleAccidentsFactory getFactory(){
		if (instance == null){
			instance = new MultipleAccidentsFactory();
		}
		return instance;
	}
	@Override
	public Insurance CreateInsurance() {
		
		return new MultipleAccidents();
	}

}
