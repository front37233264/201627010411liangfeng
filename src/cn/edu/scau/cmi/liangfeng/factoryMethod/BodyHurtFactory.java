package cn.edu.scau.cmi.liangfeng.factoryMethod;

import cn.edu.scau.cmi.liangfeng.domain.BodyHurt;
import cn.edu.scau.cmi.liangfeng.domain.Insurance;

public class BodyHurtFactory implements IFactory {

	private static BodyHurtFactory instance;
	
	public static BodyHurtFactory getFactory(){
		if (instance == null){
			instance = new BodyHurtFactory();
		}
		return instance;
	}
	@Override
	public Insurance CreateInsurance() {
	
		return new BodyHurt();
	}

}
