package cn.edu.scau.cmi.liangfeng.domain;

public class MultipleAccidents implements Insurance{

	private double money;
	private String name;
	
	public MultipleAccidents() {
		name = "MultipleAccidentsInsurance";
		money = 105000;
	}
	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getMoney() {
		return money;
	}
	@Override
	public String toString(){
		return name+" would pay you : "+ money +"$";
	}
}
