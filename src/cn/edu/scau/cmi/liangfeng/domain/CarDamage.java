package cn.edu.scau.cmi.liangfeng.domain;

public class CarDamage implements Insurance{

	private double money;
	private String name;
	
	public CarDamage() {
		name = "CarDamageInsurance";
		money = 16000;
	}
	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getMoney() {
		return money;
	}
	@Override
	public String toString(){
		return name+" would pay you : "+ money +"$";
	}

}
