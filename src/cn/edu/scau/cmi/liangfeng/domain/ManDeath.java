package cn.edu.scau.cmi.liangfeng.domain;

public class ManDeath implements Insurance{

	private double money;
	private String name;
	
	public ManDeath() {
		name = "ManDeathInsurance";
		money = 15000;
	}
	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getMoney() {
		return money;
	}
	@Override
	public String toString(){
		return name+" would pay you : "+ money +"$";
	}
}
