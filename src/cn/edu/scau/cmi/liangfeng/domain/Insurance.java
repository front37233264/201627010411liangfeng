package cn.edu.scau.cmi.liangfeng.domain;

public interface Insurance {
	String getName();
	double getMoney();
}
