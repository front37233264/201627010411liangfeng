package cn.edu.scau.cmi.liangfeng.domain;

public class BodyHurt implements Insurance{

	private double money;
	private String name;
	
	public BodyHurt() {
		money = 20000;
		name = "BodyHurtInsurance";
	}
	
	@Override
	public double getMoney(){
		return money;
	}
	
	@Override 
	public String getName(){
		return name;
	}
	
	@Override
	public String toString(){
		return name+" would pay you : "+ money +"$";
	}
}
