package cn.edu.scau.cmi.liangfeng.simpleFactoryMethod;

import cn.edu.scau.cmi.liangfeng.domain.BodyHurt;
import cn.edu.scau.cmi.liangfeng.domain.CarDamage;
import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.domain.ManDeath;
import cn.edu.scau.cmi.liangfeng.domain.MultipleAccidents;

public class InsuranceFactory {

	//"身体受伤", "汽车损坏", "人员伤亡", "多种事故"
	public static Insurance getInstance(String name){
		
		switch (name) {
		case "多种事故":
			return new MultipleAccidents();
		case "人员伤亡":
			return new ManDeath();
		case "身体受伤":
			return new BodyHurt();
		case "汽车损坏":
			return new CarDamage();

		default:
			return null;
		}
	}
}
