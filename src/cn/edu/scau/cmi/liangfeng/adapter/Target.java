package cn.edu.scau.cmi.liangfeng.adapter;

public interface Target {
	
	String doTarget(String request);

}
