package cn.edu.scau.cmi.liangfeng.adapter;

public class AdapterOfObject implements Target{
	
	private Adaptee adaptee;
	
	public AdapterOfObject() {
		this.adaptee = new Adaptee();
	}

	@Override
	public String doTarget(String request) {
		// TODO Auto-generated method stub
		return this.adaptee.dowork(request);
	}
}
