package cn.edu.scau.cmi.liangfeng.adapter;

import cn.edu.scau.cmi.liangfeng.adapter.AdapterOfClass;
import cn.edu.scau.cmi.liangfeng.adapter.AdapterOfObject;
import cn.edu.scau.cmi.liangfeng.adapter.Target;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Target classTarget=new AdapterOfClass();
		classTarget.doTarget("fix car");
		
		Target objectTarget=new AdapterOfObject();
		objectTarget.doTarget("fix plane");
	}

}
