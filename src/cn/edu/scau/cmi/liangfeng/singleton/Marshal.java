package cn.edu.scau.cmi.liangfeng.singleton;

import java.util.ArrayList;
import java.util.Iterator;

public class Marshal {

	private static final int N = 10;
	private String name;
	
	private static ArrayList<Marshal> list = new ArrayList<Marshal>();
	
	private Marshal(String name){
		this.name = name;
	}
	
	public static ArrayList<Marshal> addInstance(String name){
		if (list.size()<N){
			list.add(new Marshal(name));
		}else{
			System.out.println("已经满了 ");
		}
		return list;
	}
	
	public static Marshal getInstance(String name) {
		
		Iterator<Marshal> iterator = list.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getName() == name){
				return iterator.next();
			}
		}
		System.out.println("");
		return null;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
