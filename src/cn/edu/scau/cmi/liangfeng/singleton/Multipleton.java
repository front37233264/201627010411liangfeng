package cn.edu.scau.cmi.liangfeng.singleton;

import java.util.ArrayList;


public class Multipleton {

	public static final int N = 10;
	public static int cur = 0;
	private String name;
	
	private static ArrayList<Multipleton> list = new ArrayList<Multipleton>();
	
	private Multipleton(String name){
		this.name = name;
	}
	
	public static ArrayList<Multipleton> addInstance(String name){
		if (list.size()<N){
			for (Multipleton multipleton : list) {
				if (multipleton.getName().compareTo(name)==0){
					return null;
				}
			}
			list.add(new Multipleton(name));
			cur++;
		}else{
			System.out.println("实例已经满了");
		}
		return list;
	}
	
	public static Multipleton getInstanceBy(String name) {
		for (Multipleton multipleton : list) {
			if (multipleton.getName().compareTo(name)==0){
				return multipleton;
			}
		}
		return null;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return "i am "+getName()+" welcome to see you!";
	}
}
