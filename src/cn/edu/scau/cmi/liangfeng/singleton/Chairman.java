package cn.edu.scau.cmi.liangfeng.singleton;

public class Chairman {
	
	private String name;
	private static Chairman instance;
	
	private Chairman(String name) {
		this.name = name;
	}
	public static Chairman getInstance(String name) {
		if (instance == null){
			instance = new Chairman(name);
		}
		return instance;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void excuteOfDuty(){
		System.out.println("ͬdoing work");
	}
}
