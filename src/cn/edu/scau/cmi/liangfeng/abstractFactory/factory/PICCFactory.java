package cn.edu.scau.cmi.liangfeng.abstractFactory.factory;

import cn.edu.scau.cmi.liangfeng.domain.BodyHurt;
import cn.edu.scau.cmi.liangfeng.domain.CarDamage;
import cn.edu.scau.cmi.liangfeng.domain.ManDeath;
import cn.edu.scau.cmi.liangfeng.domain.MultipleAccidents;

public class PICCFactory extends AbstractFactory {
	
	private static PICCFactory instance;
	public static PICCFactory getFactoryInstance(){
		if (instance==null){
			instance = new PICCFactory();
		}
		return instance;
	}
	
	@Override
	public CarDamage CreateCarDamageInsurance() {
		// TODO Auto-generated method stub
		return new CarDamage();
	}

	@Override
	public ManDeath CreateManDeathInsurance() {
		// TODO Auto-generated method stub
		return new ManDeath();
	}

	@Override
	public MultipleAccidents CreateMultipleAccidentsInsurance() {
		// TODO Auto-generated method stub
		return new MultipleAccidents();
	}

	@Override
	public BodyHurt CreateBodyHurtInsurance() {
		// TODO Auto-generated method stub
		return new BodyHurt();
	}
}
