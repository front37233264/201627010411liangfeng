package cn.edu.scau.cmi.liangfeng.abstractFactory.factory;

import cn.edu.scau.cmi.liangfeng.domain.BodyHurt;
import cn.edu.scau.cmi.liangfeng.domain.CarDamage;
import cn.edu.scau.cmi.liangfeng.domain.ManDeath;
import cn.edu.scau.cmi.liangfeng.domain.MultipleAccidents;

public abstract class AbstractFactory {
	
	public static AbstractFactory getFactory(String name){
		switch (name) {
		case "PICC":
			return PICCFactory.getFactoryInstance();
		case "PinAn":
			return PinAnFactory.getFactoryInstance();
		default:
			return null;
		}
	}
	
	public abstract CarDamage CreateCarDamageInsurance();
	public abstract ManDeath CreateManDeathInsurance();
	public abstract MultipleAccidents CreateMultipleAccidentsInsurance();
	public abstract BodyHurt CreateBodyHurtInsurance();
}
