package cn.edu.scau.cmi.liangfeng.client;

import java.util.Scanner;

import javax.swing.event.ChangeListener;

import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.simpleFactoryMethod.InsuranceFactory;
import cn.edu.scau.cmi.liangfeng.singleton.Chairman;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class SimpleFactoryMethodClient extends Application{

	public static void main(String[] args) {
//		Scanner input = new Scanner(System.in);
//		String string;
//		Insurance insurance;
//		int n=5;
//		while (n>0) {
//			string = input.nextLine();
//			insurance = InsuranceFactory.getInstance(string);
//			if(insurance!=null)
//				System.out.println(insurance.toString());
//			else
//				System.out.println("please input again!");
//		}
//		input.close();
		
		Application.launch();

	}
	private Insurance insurance;

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("简单工厂");
		Group root = new Group();
        Scene scene = new Scene(root, 540, 250, Color.AQUAMARINE);
        
        Image image = new Image("image/102.jpg");
		ImageView imageView = new ImageView();
		imageView.setImage(image);
		imageView.setFitHeight(150);
		imageView.setFitWidth(200);
        
        Label nameLabel = new Label("姓名:");
        Label name = new Label("梁锋");
        
        Label idLabel = new Label("学号:");
        Label id = new Label("201627010411");
        
        Label insuranceLable = new Label("请选择保险");
        
        TextField textField = new TextField("");
        textField.setPrefSize(100, 100);
        
        final String[] choices = {"身体受伤", "汽车损坏", "人员伤亡", "多种事故"};
        final ChoiceBox<String> cb = new ChoiceBox<String>(
                FXCollections.observableArrayList("身体受伤", "汽车损坏", "人员伤亡", "多种事故"));

        cb.getSelectionModel().selectedIndexProperty()
        .addListener(
            (ObservableValue<? extends Number> oValue , Number oldValue,
            		Number newValue) ->{
//            			insuranceLable.setText(choices[newValue.intValue()]);
            			insurance = InsuranceFactory.getInstance(choices[newValue.intValue()]);
            			textField.setText(insurance.toString());
            		}
		);
        
        
        
        GridPane grid = new GridPane();
        grid.setVgap(3);
        grid.setHgap(4);
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.add(nameLabel, 0, 0);
        grid.add(name, 1, 0);
        grid.add(idLabel, 2, 0);
        grid.add(id, 3, 0);
        grid.add(insuranceLable, 0, 1);
        grid.add(cb, 1, 1);
        
        
        HBox hBox = new HBox();
        hBox.getChildren().add(imageView);
        hBox.getChildren().add(grid);
        
        VBox vBox = new VBox();
        vBox.getChildren().add(hBox);
        vBox.getChildren().add(textField);
        
        root.getChildren().add(vBox);
        primaryStage.setScene(scene);
        primaryStage.show();
	}

}
