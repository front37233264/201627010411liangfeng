package cn.edu.scau.cmi.liangfeng.client;

import java.beans.beancontext.BeanContext;

import cn.edu.scau.cmi.liangfeng.abstractFactory.factory.AbstractFactory;
import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.factoryMethod.BodyHurtFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.CarDamgeFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.IFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.ManDeathFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.MultipleAccidentsFactory;

public class AbstractFactoryMethodClient {

	public static void main(String[] args) {
//		"身体受伤", "人员伤亡", "汽车损坏", "多种事故"
		String factoryname = "PICC";
		AbstractFactory factory = AbstractFactory.getFactory(factoryname);
		Insurance insurance = null;
		String insurancename = "身体受伤";
		switch (insurancename) {
		case "身体受伤":insurance = factory.CreateBodyHurtInsurance();break;
		case "人员伤亡":insurance = factory.CreateManDeathInsurance();break;
		case "汽车损坏":insurance = factory.CreateCarDamageInsurance();break;
		case "多种事故":insurance = factory.CreateMultipleAccidentsInsurance();break;
		default:
			break;
		}
		
		
		System.out.println(insurance.getName()+insurance.getMoney());
	

	}

}
