package cn.edu.scau.cmi.liangfeng.client;

import cn.edu.scau.cmi.liangfeng.interProgramming.Apple;
import cn.edu.scau.cmi.liangfeng.interProgramming.Banana;
import cn.edu.scau.cmi.liangfeng.interProgramming.Fruit;

public class InterProgrammingClient {

	public static void main(String[] args) {
		Fruit apple = new Apple();
		apple.color();
		
		Fruit banana = new Banana();
		banana.color();
		
		System.out.println("ok");
	}

}
