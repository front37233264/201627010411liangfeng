package cn.edu.scau.cmi.liangfeng.client;

import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.factoryMethod.BodyHurtFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.CarDamgeFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.IFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.ManDeathFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.MultipleAccidentsFactory;

public class FactoryMethodClient {

	public static void main(String[] args) {
//		"身体受伤", "人员伤亡", "汽车损坏", "多种事故"
		String factoryname = "人员伤亡";
		IFactory factory = null;
		switch (factoryname) {
		case "身体受伤":factory = new BodyHurtFactory();break;
		case "人员伤亡":factory = new ManDeathFactory();break;
		case "汽车损坏":factory = new CarDamgeFactory();break;
		case "多种事故":factory = new MultipleAccidentsFactory();break;
		default:
			break;
		}
		
		Insurance insurance = factory.CreateInsurance();
		System.out.println(insurance.getName()+insurance.getMoney());
	}

}
