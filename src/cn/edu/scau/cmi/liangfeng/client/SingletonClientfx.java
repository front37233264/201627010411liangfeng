package cn.edu.scau.cmi.liangfeng.client;
import cn.edu.scau.cmi.liangfeng.singleton.Chairman;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SingletonClientfx extends Application{

	private Chairman chairman;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application.launch();
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		 primaryStage.setTitle("Drawing Text");
	        Group root = new Group();
	        Scene scene = new Scene(root, 450, 100, Color.AQUAMARINE);
	        
	        Button buildChairmanbt = new Button("创建主席");
	        
	        Button getInstancebt = new Button("获取主席实例");
	        
	        
	        TextField nametext = new TextField("请输入名字");
	        
	        Label name = new Label("名字:");
	        Label line = new Label();
	        Label instance = new Label("null");
	        
	        GridPane grid = new GridPane();
	        grid.setVgap(4);
	        grid.setHgap(10);
	        grid.setPadding(new Insets(5, 5, 5, 5));
	        grid.add(name, 0, 0);
	        grid.add(nametext, 1, 0);
	        grid.add(buildChairmanbt, 2, 0);
	        grid.add(line, 1, 2);
	        grid.add(instance, 2, 3);
	        grid.add(getInstancebt, 1, 3);
	        
	        buildChairmanbt.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					if (!nametext.getText().equals("请输入名字")){
						chairman = Chairman.getInstance(nametext.getText());
						line.setText("主席创建成功!");
					}
					
				}
			});
	        
	        getInstancebt.setOnAction(new EventHandler<ActionEvent>() {
	        	
	        	@Override
	        	public void handle(ActionEvent event){
	        		if (chairman!=null){
	        			instance.setText("成功获得 "+chairman.getName()+" 实例");
	        		}
	        		
	        	}
			});
	        
	        root.getChildren().add(grid);
	        primaryStage.setScene(scene);
	        primaryStage.show();
	}

}
