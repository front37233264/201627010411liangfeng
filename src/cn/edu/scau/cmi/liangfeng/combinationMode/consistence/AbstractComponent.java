package cn.edu.scau.cmi.liangfeng.combinationMode.consistence;


public abstract class AbstractComponent {
	public abstract String dowork();
	public abstract String add(AbstractComponent c);
	public abstract String remove(AbstractComponent c);
	public abstract String changePersonWork(int i);
}
