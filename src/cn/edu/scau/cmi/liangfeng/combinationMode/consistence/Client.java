package cn.edu.scau.cmi.liangfeng.combinationMode.consistence;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AbstractComponent person = new Person();
		System.out.println(person.dowork());
		person.add(new Person());
		person.add(new Organization());
		person.remove(new Person());
		person.changePersonWork(2);
		
		AbstractComponent organization = new Organization();
		System.out.println(organization.dowork());
		organization.add(new Person());
		organization.remove(new Person());
		organization.changePersonWork(2);
		organization.add(new Organization());
	}

}
