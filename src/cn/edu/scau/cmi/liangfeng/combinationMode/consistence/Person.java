package cn.edu.scau.cmi.liangfeng.combinationMode.consistence;


public class Person extends AbstractComponent {

	@Override
	public String dowork() {
		// TODO Auto-generated method stub
		return "i am Person, i am doing work";
	}
	@Override
	public String add(AbstractComponent c){
		//System.out.println("i am organization,successful add Child");
		return "sorry, You don't have enough authority to add Person!";
	}
	@Override
	public String remove(AbstractComponent c){
		//System.out.println("i am organization,successful remove Child");
		return "sorry, You don't have enough authority to remove person!";
	}
	@Override
	public String changePersonWork(int i){
		//System.out.println("i am organization,successful getChild");
		return "sorry, You don't have enough authority to change person work!";
	}

}
