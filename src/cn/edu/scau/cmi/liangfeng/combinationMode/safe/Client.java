package cn.edu.scau.cmi.liangfeng.combinationMode.safe;

public class Client {

	public static void main(String[] args) {
		AbstractComponent person = new Person();
		System.out.println(person.dowork());
		
		Organization organization = new Organization();
		System.out.println(organization.dowork());
		organization.add(person);
		organization.remove(person);
		organization.changePersonWork(1);
		
	}

}
