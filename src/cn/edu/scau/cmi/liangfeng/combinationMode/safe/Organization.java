package cn.edu.scau.cmi.liangfeng.combinationMode.safe;

import java.util.ArrayList;

public class Organization extends AbstractComponent {

	private ArrayList<AbstractComponent> list = new ArrayList<>();
	@Override
	public String dowork() {
		// TODO Auto-generated method stub
		return "i am organization, i am doing work";
	}
	public String add(AbstractComponent c){
		//System.out.println("i am organization,successful add Child");
		return "i am organization,successful add Person";
	}
	public String remove(AbstractComponent c){
		//System.out.println("i am organization,successful remove Child");
		return "i am organization,successful remove Person";
	}
	public String changePersonWork(int i){
		//System.out.println("i am organization,successful getChild");
		return "i am organization,successful change person work";
	}

}
