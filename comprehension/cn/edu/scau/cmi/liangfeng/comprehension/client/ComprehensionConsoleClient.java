package cn.edu.scau.cmi.liangfeng.comprehension.client;

import org.springframework.context.ApplicationContext;

import cn.edu.scau.cmi.liangfeng.comprehension.dao.StudentDAO;
import cn.edu.scau.cmi.liangfeng.comprehension.dao.TeacherDAO;
import cn.edu.scau.cmi.liangfeng.comprehension.domain.Student;
import cn.edu.scau.cmi.liangfeng.comprehension.domain.Teacher;
import cn.edu.scau.cmi.liangfeng.comprehension.util.ComprehensionApplicationContextUtil;

public class ComprehensionConsoleClient {

	public static void main(String[] args) {
//		（1） 初始化applicationContext，并获取studentDAO对象
		ApplicationContext applicationContext = ComprehensionApplicationContextUtil.getApplicationContext();
		StudentDAO studentDAO=(StudentDAO) applicationContext.getBean("studentDAO");
		TeacherDAO teacherDAO=(TeacherDAO) applicationContext.getBean("teacherDAO");
//		（2）创建教师和学生对象，并设置他们之间的关系
		Teacher zhukai=new Teacher();
		zhukai.setName("老李");
		teacherDAO.save(zhukai);
		
//		
		Student wangwu=new Student();
		wangwu.setName("小王");
		
		wangwu.setTeacher(zhukai);
//		（3）保存数据
		studentDAO.save(wangwu);
	}
}