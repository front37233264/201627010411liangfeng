package cn.edu.scau.cmi.liangfeng.client;

import java.util.ArrayList;

import cn.edu.scau.cmi.liangfeng.abstractFactory.factory.AbstractFactory;
import cn.edu.scau.cmi.liangfeng.abstractFactory.factory.PICCFactory;
import cn.edu.scau.cmi.liangfeng.abstractFactory.factory.PinAnFactory;
import cn.edu.scau.cmi.liangfeng.adapter.AdapterOfClass;
import cn.edu.scau.cmi.liangfeng.adapter.AdapterOfObject;
import cn.edu.scau.cmi.liangfeng.combinationMode.safe.Organization;
import cn.edu.scau.cmi.liangfeng.combinationMode.safe.Person;
import cn.edu.scau.cmi.liangfeng.domain.Insurance;
import cn.edu.scau.cmi.liangfeng.factoryMethod.BodyHurtFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.CarDamgeFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.ManDeathFactory;
import cn.edu.scau.cmi.liangfeng.factoryMethod.MultipleAccidentsFactory;
import cn.edu.scau.cmi.liangfeng.interProgramming.Apple;
import cn.edu.scau.cmi.liangfeng.interProgramming.Banana;
import cn.edu.scau.cmi.liangfeng.interProgramming.Fruit;
import cn.edu.scau.cmi.liangfeng.simpleFactoryMethod.InsuranceFactory;
import cn.edu.scau.cmi.liangfeng.singleton.Chairman;
import cn.edu.scau.cmi.liangfeng.singleton.Multipleton;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class FinalClientByFx extends Application{

	public static void main(String[] args) {
		
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane root = new Pane();
		Scene scene = new Scene(root, 770, 400);
		
		Image image = new Image("image/102.jpg");
		ImageView headImage = new ImageView(image);
		headImage.setFitWidth(300);
		headImage.setFitHeight(280);
		Label labelId = new Label("学号：\t201627010411");
		Label labelName = new Label("姓名：\t梁锋");
		Button bt1_1 = new Button("实验一：单例模式");
		Button bt1_2 = new Button("实验一：多例模式");
		Button bt1_3 = new Button("实验一：接口编程");
		Button bt2_1 = new Button("实验二：简单工厂");
		Button bt2_2 = new Button("实验二：工厂模式");
		Button bt2_3 = new Button("实验二：抽象工厂");
		Button bt3_1 = new Button("实验三：适配器(对象)");
		Button bt3_2 = new Button("实验三：适配器(类)");
		Button bt4_1 = new Button("实验四：安全组合模式");
		Button bt4_2 = new Button("实验四：一致性组合模式");
		
		bt1_1.setOnAction(new EventHandler<ActionEvent>() {
			private Chairman chairman=null;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				
				stage.setTitle("实验一：单例模式");
		        Group root = new Group();
		        Scene scene = new Scene(root, 450, 100, Color.WHITE);
		        
		        Button buildChairmanbt = new Button("创建主席");
		        Button getInstancebt = new Button("获取主席实例");

		        TextField nametext = new TextField("请输入名字");
		        
		        Label name = new Label("名字:");
		        Label line = new Label();
		        Label instance = new Label("null");
		        
		        GridPane grid = new GridPane();
		        grid.setVgap(4);
		        grid.setHgap(10);
		        grid.setPadding(new Insets(5, 5, 5, 5));
		        grid.add(name, 0, 0);
		        grid.add(nametext, 1, 0);
		        grid.add(buildChairmanbt, 2, 0);
		        grid.add(line, 1, 2);
		        grid.add(instance, 2, 3);
		        grid.add(getInstancebt, 1, 3);
		        
		        buildChairmanbt.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {

						if (!nametext.getText().equals("请输入名字")){
							chairman = Chairman.getInstance(nametext.getText());
							line.setText("主席创建成功!");
						}
						
					}
				});
		        
		        getInstancebt.setOnAction(new EventHandler<ActionEvent>() {
		        	
		        	@Override
		        	public void handle(ActionEvent event){
		        		if (chairman!=null){
		        			instance.setText("成功获得 "+chairman.getName()+" 实例");
		        		}
		        		
		        	}
				});
		        
		        root.getChildren().add(grid);
		        stage.setScene(scene);
		        stage.show();
			}
		});
		
		bt1_2.setOnAction(new EventHandler<ActionEvent>() {
			public ArrayList<Multipleton> list = null;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验一：多例模式");
				Pane pane = new Pane();
				Scene scene = new Scene(pane, 358, 370);
				Label labelname = new Label("请输入实例的名称:");
				labelname.setLayoutX(25);
				labelname.setLayoutY(20);
				TextField textField = new TextField();
				textField.setLayoutX(150);
				textField.setLayoutY(15);
				Button btCreate = new Button("生成该实例");
				btCreate.setLayoutX(23);
				btCreate.setLayoutY(69);
				Button btGet = new Button("通过名称获取实例");
				btGet.setLayoutX(168);
				btGet.setLayoutY(69);
				Label labelmessage = new Label();
				labelmessage.setLayoutX(27);
				labelmessage.setLayoutY(119);
				TextArea tArea = new TextArea();
				tArea.setLayoutX(23);
				tArea.setLayoutY(154);
				tArea.setPrefSize(315, 144);
				Button btOutput = new Button("输出所有实例信息");
				btOutput.setLayoutX(115);
				btOutput.setLayoutY(311);
				
				btCreate.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						if (textField.getText()==null) return;
						if (Multipleton.N>Multipleton.cur){
							ArrayList<Multipleton> temp = Multipleton.addInstance(textField.getText());
							if (temp==null){
								labelmessage.setText("failed to create "+textField.getText()+" 可能已存在此实例");
							}else {
								list = temp;
								labelmessage.setText("successfully to create instance "+textField.getText());
							}
							
						}else {
							labelmessage.setText("实例已经超出数量，不可再生成实例了");
						}
					}
				});
				btGet.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						if (textField.getText()==null) return;
						Multipleton multipleton = Multipleton.getInstanceBy(textField.getText().trim());
						if (multipleton!=null){
							tArea.setText(multipleton.toString());
							labelmessage.setText("get instance successfully");
						}else {
							labelmessage.setText("找不到该实例，可能不存在");
						}
					}
				});
				
				btOutput.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						if (list ==null || list.size()==0) return;
						tArea.setText("");
						for (Multipleton multipleton : list) {
							tArea.appendText(multipleton.toString()+"\n\r");
						}
					}
				});
				pane.getChildren().addAll(labelname,textField,btCreate,btGet,labelmessage,
						tArea,btOutput);
				stage.setScene(scene);
				stage.show();
			}
		});
		
		bt1_3.setOnAction(new EventHandler<ActionEvent>() {

			public Fruit fruit = null;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验一：接口编程");
				Button btCreateApple = new Button("生成苹果");
				btCreateApple.setLayoutX(60);
				btCreateApple.setLayoutY(46);
				Button btCreateBanana = new Button("生成香蕉");
				btCreateBanana.setLayoutX(241);
				btCreateBanana.setLayoutY(46);
				Button btOutput = new Button("输出水果信息");
				btOutput.setLayoutX(123);
				btOutput.setLayoutY(222);
				Pane pane = new Pane();
				Scene scene = new Scene(pane,370,275);
				Label message = new Label("");
				message.setLayoutX(81);
				message.setLayoutY(105);
				TextField textField = new TextField();
				textField.setLayoutX(79);
				textField.setLayoutY(141);
				textField.setPrefWidth(225);
				textField.setPrefHeight(63);
				
				
				btCreateApple.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						fruit = new Apple();
						message.setText("successfully to create apple");
					}
				});
				btCreateBanana.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						fruit = new Banana();
						message.setText("successfully to create banana");
					}
				});
				btOutput.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						if (fruit==null) return;
						textField.setText(fruit.color());
					}
				});
				
				pane.getChildren().addAll(btCreateApple,btCreateBanana,message,
						textField,btOutput);
				stage.setScene(scene);
				stage.show();
				
			}
		});
		
		bt2_1.setOnAction(new EventHandler<ActionEvent>() {
			private Insurance insurance;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验二：简单工厂");
				Group root = new Group();
		        Scene scene = new Scene(root, 540, 250, Color.WHITE);
		        
		        Image image = new Image("image/102.jpg");
				ImageView imageView = new ImageView();
				imageView.setImage(image);
				imageView.setFitHeight(150);
				imageView.setFitWidth(200);
		        
		        Label nameLabel = new Label("姓名:");
		        Label name = new Label("梁锋");
		        
		        Label idLabel = new Label("学号:");
		        Label id = new Label("201627010411");
		        
		        Label insuranceLable = new Label("请选择保险");
		        
		        TextField textField = new TextField("");
		        textField.setPrefSize(100, 100);
		        
		        final String[] choices = {"身体受伤", "汽车损坏", "人员伤亡", "多种事故"};
		        final ChoiceBox<String> cb = new ChoiceBox<String>(
		                FXCollections.observableArrayList("身体受伤", "汽车损坏", "人员伤亡", "多种事故"));

		        cb.getSelectionModel().selectedIndexProperty()
		        .addListener(
		            (ObservableValue<? extends Number> oValue , Number oldValue,
		            		Number newValue) ->{
//		            			insuranceLable.setText(choices[newValue.intValue()]);
		            			insurance = InsuranceFactory.getInstance(choices[newValue.intValue()]);
		            			textField.setText(insurance.toString());
		            		}
				);
		        
		        
		        
		        GridPane grid = new GridPane();
		        grid.setVgap(3);
		        grid.setHgap(4);
		        grid.setPadding(new Insets(5, 5, 5, 5));
		        grid.add(nameLabel, 0, 0);
		        grid.add(name, 1, 0);
		        grid.add(idLabel, 2, 0);
		        grid.add(id, 3, 0);
		        grid.add(insuranceLable, 0, 1);
		        grid.add(cb, 1, 1);
		        
		        
		        HBox hBox = new HBox();
		        hBox.getChildren().add(imageView);
		        hBox.getChildren().add(grid);
		        
		        VBox vBox = new VBox();
		        vBox.getChildren().add(hBox);
		        vBox.getChildren().add(textField);
		        
		        root.getChildren().add(vBox);
		        stage.setScene(scene);
		        stage.show();
			}
		});

		bt2_2.setOnAction(new EventHandler<ActionEvent>() {
			public Insurance insurance;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验二：工厂模式");
				Group root = new Group();
		        Scene scene = new Scene(root, 540, 250, Color.WHITE);
		        
		        Image image = new Image("image/102.jpg");
				ImageView imageView = new ImageView();
				imageView.setImage(image);
				imageView.setFitHeight(160);
				imageView.setFitWidth(150);
		        
		        Label nameLabel = new Label("姓名:");
		        Label name = new Label("梁锋");
		        
		        Label idLabel = new Label("学号:");
		        Label id = new Label("201627010411");
		        
		        Label insuranceLable = new Label("请选择保险");
		        
		        TextField textField = new TextField("");
		        textField.setPrefSize(100, 100);
		        
		        final String[] choices = {"身体受伤", "汽车损坏", "人员伤亡", "多种事故"};
		        final ChoiceBox<String> cb = new ChoiceBox<String>(
		                FXCollections.observableArrayList("身体受伤", "汽车损坏", "人员伤亡", "多种事故"));

		        cb.getSelectionModel().selectedIndexProperty()
		        .addListener(
		            (ObservableValue<? extends Number> oValue , Number oldValue,
		            		Number newValue) ->{
//		            			insuranceLable.setText(choices[newValue.intValue()]);
		            			//insurance = InsuranceFactory.getInstance(choices[newValue.intValue()]);
		            			switch (choices[newValue.intValue()]) {
		            			case "多种事故":
		            				insurance = MultipleAccidentsFactory.getFactory().CreateInsurance();
		            				break;
		            			case "人员伤亡":
		            				insurance = ManDeathFactory.getFactory().CreateInsurance();
		            				break;
		            			case "身体受伤":
		            				insurance = BodyHurtFactory.getFactory().CreateInsurance();
		            				break;
		            			case "汽车损坏":
		            				insurance = CarDamgeFactory.getFactory().CreateInsurance();
		            				break;
		            			default:
		            				insurance = null;
		            				break;
		            			}
		            			if (insurance!=null)
		            				textField.setText(insurance.toString());
		            			
		            		}
				);
		        GridPane grid = new GridPane();
		        grid.setVgap(3);
		        grid.setHgap(4);
		        grid.setPadding(new Insets(5, 5, 5, 5));
		        grid.add(nameLabel, 0, 0);
		        grid.add(name, 1, 0);
		        grid.add(idLabel, 2, 0);
		        grid.add(id, 3, 0);
		        grid.add(insuranceLable, 0, 1);
		        grid.add(cb, 1, 1);
		        
		        
		        HBox hBox = new HBox();
		        hBox.getChildren().add(imageView);
		        hBox.getChildren().add(grid);
		        
		        VBox vBox = new VBox();
		        vBox.getChildren().add(hBox);
		        vBox.getChildren().add(textField);
		        
		        root.getChildren().add(vBox);
		        stage.setScene(scene);
		        stage.show();
				
			}
		});

		bt2_3.setOnAction(new EventHandler<ActionEvent>() {

			public Insurance insurance;
			public AbstractFactory abstractFactory;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验二：抽象工厂");
				Group root = new Group();
		        Scene scene = new Scene(root, 540, 250, Color.WHITE);
		        
		        Image image = new Image("image/102.jpg");
				ImageView imageView = new ImageView();
				imageView.setImage(image);
				imageView.setFitHeight(150);
				imageView.setFitWidth(200);
		        
		        Label nameLabel = new Label("姓名:");
		        Label name = new Label("梁锋");
		        
		        Label idLabel = new Label("学号:");
		        Label id = new Label("201627010411");
		        
		        Label insuranceLable = new Label("请选择保险");
		        Label companyLable = new Label("请选择公司");
		        
		        TextField textField = new TextField("");
		        textField.setPrefSize(100, 100);
		        
		        final String[] choices = {"身体受伤", "汽车损坏", "人员伤亡", "多种事故"};
		        final ChoiceBox<String> cb = new ChoiceBox<String>(
		                FXCollections.observableArrayList("身体受伤", "汽车损坏", "人员伤亡", "多种事故"));

		        cb.getSelectionModel().selectedIndexProperty()
		        .addListener(
		            (ObservableValue<? extends Number> oValue , Number oldValue,
		            		Number newValue) ->{
		            			if (abstractFactory == null) return;
		            			if (abstractFactory instanceof PICCFactory){
		            				switch (choices[newValue.intValue()]) {
			            			case "多种事故":
			            				insurance = PICCFactory.getFactoryInstance().CreateMultipleAccidentsInsurance();
			            				break;
			            			case "人员伤亡":
			            				insurance = PICCFactory.getFactoryInstance().CreateManDeathInsurance();
			            				break;
			            			case "身体受伤":
			            				insurance = PICCFactory.getFactoryInstance().CreateBodyHurtInsurance();
			            				break;
			            			case "汽车损坏":
			            				insurance = PICCFactory.getFactoryInstance().CreateCarDamageInsurance();
			            				break;
			            			default:
			            				insurance = null;
			            				break;
			            			}
		            				if (insurance!=null) textField.setText(insurance.toString()+" from PICC");
		            			}else {
		            				switch (choices[newValue.intValue()]) {
			            			case "多种事故":
			            				insurance = PinAnFactory.getFactoryInstance().CreateMultipleAccidentsInsurance();
			            				break;
			            			case "人员伤亡":
			            				insurance = PinAnFactory.getFactoryInstance().CreateManDeathInsurance();
			            				break;
			            			case "身体受伤":
			            				insurance = PinAnFactory.getFactoryInstance().CreateBodyHurtInsurance();
			            				break;
			            			case "汽车损坏":
			            				insurance = PinAnFactory.getFactoryInstance().CreateCarDamageInsurance();
			            				break;
			            			default:
			            				insurance = null;
			            				break;
			            			}
		            				if (insurance!=null) textField.setText(insurance.toString()+" from PinAn");
								}
		            			
		            		}
				);
		        final String[] choices2 = {"PICC", "PinAn"};
		        final ChoiceBox<String> cb2 = new ChoiceBox<String>(
		                FXCollections.observableArrayList("PICC", "PinAn"));

		        cb2.getSelectionModel().selectedIndexProperty()
		        .addListener(
		            (ObservableValue<? extends Number> oValue , Number oldValue,
		            		Number newValue) ->{
//		            			insuranceLable.setText(choices[newValue.intValue()]);
//		            			insurance = InsuranceFactory.getInstance(choices[newValue.intValue()]);
		            			
		            			switch (choices2[newValue.intValue()]) {
								case "PICC":
									abstractFactory = AbstractFactory.getFactory("PICC");
									break;
							    case "PinAn":
							    	abstractFactory = AbstractFactory.getFactory("PinAn");
							    	break;
								default:
									break;
								}
		            			
		            		}
				);
		        
		        
		        GridPane grid = new GridPane();
		        grid.setVgap(3);
		        grid.setHgap(4);
		        grid.setPadding(new Insets(5, 5, 5, 5));
		        grid.add(nameLabel, 0, 0);
		        grid.add(name, 1, 0);
		        grid.add(idLabel, 2, 0);
		        grid.add(id, 3, 0);
		        grid.add(companyLable, 0, 1);
		        grid.add(cb2, 1, 1);
		        grid.add(insuranceLable, 0, 2);
		        grid.add(cb, 1, 2);
		        
		        
		        HBox hBox = new HBox();
		        hBox.getChildren().add(imageView);
		        hBox.getChildren().add(grid);
		        
		        VBox vBox = new VBox();
		        vBox.getChildren().add(hBox);
		        vBox.getChildren().add(textField);
		        
		        root.getChildren().add(vBox);
		        stage.setScene(scene);
		        stage.show();
			}
		});
		
		bt3_1.setOnAction(new EventHandler<ActionEvent>() {
			public String result;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验三：适配器(对象)");
				Group root = new Group();
		        Scene scene = new Scene(root, 490, 250, Color.WHITE);
		        
		        Image image = new Image("image/tool.jpg");
				ImageView imageView = new ImageView();
				imageView.setImage(image);
				imageView.setFitHeight(150);
				imageView.setFitWidth(200);
		        
		        Label nameLabel = new Label("姓名:梁锋");
		        Label idLabel = new Label("学号:201627010411");
		        Label choosetypeLable = new Label("请选择发动机类型：");
		        
		        TextField textField = new TextField("");
		        textField.setPrefSize(250, 100);
		        
		        final String[] choices = {"plane", "car", "bus", "motorcycle"};
		        final ChoiceBox<String> cb = new ChoiceBox<String>(
		                FXCollections.observableArrayList("plane", "car", "bus", "motorcycle"));

		        cb.getSelectionModel().selectedIndexProperty()
		        .addListener(
		            (ObservableValue<? extends Number> oValue , Number oldValue,
		            		Number newValue) ->{
//		            			insuranceLable.setText(choices[newValue.intValue()]);
//		            			insurance = InsuranceFactory.getInstance(choices[newValue.intValue()]);
//		            			textField.setText(insurance.toString());
		            			result = new AdapterOfObject().doTarget(choices[newValue.intValue()]);
		            		}
				);
		        Button buttonfix = new Button("开始适配并修理");
		        buttonfix.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						if (result==null) return;
						textField.setText(result.toString());
					}
				});
		        
		        GridPane grid = new GridPane();
		        grid.setVgap(3);
		        grid.setHgap(4);
		        grid.setPadding(new Insets(5, 5, 5, 5));
		        grid.add(nameLabel, 0, 0);
		        grid.add(idLabel, 1, 0);
		        grid.add(choosetypeLable, 0, 1);
		        grid.add(cb, 1, 1);
		        grid.add(buttonfix, 1, 2);
		        
		        
		        HBox hBox = new HBox();
		        hBox.getChildren().add(imageView);
		        hBox.getChildren().add(grid);
		        
		        VBox vBox = new VBox();
		        vBox.getChildren().add(hBox);
		        vBox.getChildren().add(textField);
		        
		        root.getChildren().add(vBox);
		        stage.setScene(scene);
		        stage.show();
			}
		});
		
		bt3_2.setOnAction(new EventHandler<ActionEvent>() {

			public String result;
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				stage.setTitle("实验三：适配器(类)");
				Group root = new Group();
		        Scene scene = new Scene(root, 490, 250, Color.WHITE);
		        
		        Image image = new Image("image/tool.jpg");
				ImageView imageView = new ImageView();
				imageView.setImage(image);
				imageView.setFitHeight(150);
				imageView.setFitWidth(200);
		        
		        Label nameLabel = new Label("姓名:梁锋");
		        Label idLabel = new Label("学号:201627010411");
		        Label choosetypeLable = new Label("请选择发动机类型：");
		        
		        TextField textField = new TextField("");
		        textField.setPrefSize(100, 100);
		        
		        final String[] choices = {"plane", "car", "bus", "motorcycle"};
		        final ChoiceBox<String> cb = new ChoiceBox<String>(
		                FXCollections.observableArrayList("plane", "car", "bus", "motorcycle"));

		        cb.getSelectionModel().selectedIndexProperty()
		        .addListener(
		            (ObservableValue<? extends Number> oValue , Number oldValue,
		            		Number newValue) ->{
//		            			insuranceLable.setText(choices[newValue.intValue()]);
//		            			insurance = InsuranceFactory.getInstance(choices[newValue.intValue()]);
//		            			textField.setText(insurance.toString());
		            			result = new AdapterOfClass().doTarget(choices[newValue.intValue()]);
		            		}
				);
		        Button buttonfix = new Button("开始适配并修理");
		        buttonfix.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						if (result==null) return;
						textField.setText(result.toString());
					}
				});
		        
		        GridPane grid = new GridPane();
		        grid.setVgap(3);
		        grid.setHgap(4);
		        grid.setPadding(new Insets(5, 5, 5, 5));
		        grid.add(nameLabel, 0, 0);
		        grid.add(idLabel, 1, 0);
		        grid.add(choosetypeLable, 0, 1);
		        grid.add(cb, 1, 1);
		        grid.add(buttonfix, 1, 2);
		        
		        
		        HBox hBox = new HBox();
		        hBox.getChildren().add(imageView);
		        hBox.getChildren().add(grid);
		        
		        VBox vBox = new VBox();
		        vBox.getChildren().add(hBox);
		        vBox.getChildren().add(textField);
		        
		        root.getChildren().add(vBox);
		        stage.setScene(scene);
		        stage.show();
			}
		});
		
		bt4_1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				Pane pane = new Pane();
				Scene scene = new Scene(pane, 523, 345);
				stage.setTitle("实验四：安全组合模式");
				
				Label labelPerson = new Label("个体Person");
				labelPerson.setLayoutX(23);
				labelPerson.setLayoutY(44);
				Label labelOrganization = new Label("组织Organization");
				labelOrganization.setLayoutX(23);
				labelOrganization.setLayoutY(200);
				
				Button btDowork = new Button("dowork");
				Button btDowork2 = new Button("dowork");
				Button btAddPerson = new Button("addPerson");
				btAddPerson.setDisable(true);
				Button btAddPerson2 = new Button("addPerson");
				Button btRemove = new Button("removePerson");
				btRemove.setDisable(true);
				Button btRemove2 = new Button("removePerson");
				Button btChangeWork = new Button("changePersonWork");
				btChangeWork.setDisable(true);
				Button btChangeWork2 = new Button("changePersonWork");
				
				ToolBar barPerson = new ToolBar();
				barPerson.getItems().addAll(btDowork,btAddPerson,btRemove,btChangeWork);
				barPerson.setLayoutX(14);
				barPerson.setLayoutY(72);
				ToolBar barOrganization = new ToolBar();
				barOrganization.getItems().addAll(btDowork2,btAddPerson2,btRemove2,btChangeWork2);
				barOrganization.setLayoutX(23);
				barOrganization.setLayoutY(231);
				
				TextField tf_person = new TextField();
				tf_person.setLayoutX(14);
				tf_person.setLayoutY(125);
				tf_person.setPrefSize(491, 30);
				TextField tf_organization = new TextField();
				tf_organization.setLayoutX(23);
				tf_organization.setLayoutY(285);
				tf_organization.setPrefSize(491, 30);
				pane.getChildren().addAll(labelPerson,
						barPerson,tf_person,labelOrganization,
						barOrganization,tf_organization);
				
				btDowork.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_person.setText(new Person().dowork());
					}
				});
				btDowork2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new Organization().dowork());
					}
				});
				btAddPerson2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new Organization().add(null));
					}
				});
				btRemove2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new Organization().remove(null));
					}
				});
				btChangeWork2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new Organization().changePersonWork(0));
					}
				});
				
				stage.setScene(scene);
				stage.show();
			}
		});
		bt4_2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				Stage stage = new Stage();
				Pane pane = new Pane();
				Scene scene = new Scene(pane, 523, 345);
				stage.setTitle("实验四：一致性组合模式");
				
				Label labelPerson = new Label("个体Person");
				labelPerson.setLayoutX(23);
				labelPerson.setLayoutY(44);
				Label labelOrganization = new Label("组织Organization");
				labelOrganization.setLayoutX(23);
				labelOrganization.setLayoutY(200);
				
				Button btDowork = new Button("dowork");
				Button btDowork2 = new Button("dowork");
				Button btAddPerson = new Button("addPerson");
				Button btAddPerson2 = new Button("addPerson");
				Button btRemove = new Button("removePerson");
				Button btRemove2 = new Button("removePerson");
				Button btChangeWork = new Button("changePersonWork");
				Button btChangeWork2 = new Button("changePersonWork");
				
				ToolBar barPerson = new ToolBar();
				barPerson.getItems().addAll(btDowork,btAddPerson,btRemove,btChangeWork);
				barPerson.setLayoutX(14);
				barPerson.setLayoutY(72);
				ToolBar barOrganization = new ToolBar();
				barOrganization.getItems().addAll(btDowork2,btAddPerson2,btRemove2,btChangeWork2);
				barOrganization.setLayoutX(23);
				barOrganization.setLayoutY(231);
				
				TextField tf_person = new TextField();
				tf_person.setLayoutX(14);
				tf_person.setLayoutY(125);
				tf_person.setPrefSize(491, 30);
				TextField tf_organization = new TextField();
				tf_organization.setLayoutX(23);
				tf_organization.setLayoutY(285);
				tf_organization.setPrefSize(491, 30);
				pane.getChildren().addAll(labelPerson,
						barPerson,tf_person,labelOrganization,
						barOrganization,tf_organization);
				
				btDowork.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_person.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Person().dowork());
					}
				});
				btAddPerson.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_person.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Person().add(null));
					}
				});
				btRemove.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_person.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Person().remove(null));
					}
				});
				btChangeWork.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_person.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Person().changePersonWork(2));
					}
				});
				btDowork2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Organization().dowork());
					}
				});
				btAddPerson2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Organization().add(null));
					}
				});
				btRemove2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Organization().remove(null));
					}
				});
				btChangeWork2.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						// TODO Auto-generated method stub
						tf_organization.setText(new cn.edu.scau.cmi.liangfeng.combinationMode.consistence.Organization().changePersonWork(0));
					}
				});
				
				stage.setScene(scene);
				stage.show();
			}
		});
		
		
		VBox rightVBox = new VBox();
		rightVBox.setPrefWidth(200);
		rightVBox.setPrefHeight(320);
		rightVBox.setLayoutX(390);
		rightVBox.setLayoutY(28);
		rightVBox.setSpacing(10);
		rightVBox.getChildren().addAll(bt1_1,bt1_2,bt1_3,bt2_1,bt2_2,bt2_3,bt3_1,bt3_2);
		root.getChildren().add(rightVBox);
		
		VBox rightVBox2 = new VBox();
		rightVBox2.setPrefWidth(200);
		rightVBox2.setPrefHeight(320);
		rightVBox2.setLayoutX(567);
		rightVBox2.setLayoutY(28);
		rightVBox2.setSpacing(10);
		rightVBox2.getChildren().addAll(bt4_1,bt4_2);
		root.getChildren().add(rightVBox2);
		
		VBox leftVBox = new VBox();
		leftVBox.setPrefWidth(320);
		leftVBox.setPrefHeight(345);
		leftVBox.setLayoutX(30);
		leftVBox.setLayoutY(28);
		leftVBox.setSpacing(20);
		leftVBox.getChildren().addAll(headImage,labelId,labelName);
		root.getChildren().add(leftVBox);
		
		primaryStage.setTitle("综合性实验");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
