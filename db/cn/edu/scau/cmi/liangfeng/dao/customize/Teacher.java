package cn.edu.scau.cmi.liangfeng.dao.customize;
import java.util.HashSet;
import java.util.Set;

public class Teacher {

	private int id;
	private String name;
	public Set<Student> mystudents;
	
	public Teacher() {
		mystudents = new HashSet<>();
	}
	public Teacher(int id, String name) {
		this.id=id;
		this.name=name;
		mystudents = new HashSet<>();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
