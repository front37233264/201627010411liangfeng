package cn.edu.scau.cmi.liangfeng.dao.customize;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class StudentDAO {
	
	public static boolean addStudent(Student student) {
		Connection conn = DBUtil.getConn();
		Teacher tutor = student.getMytutor();
		if (tutor!=null) {
			String checktidsql = "select * from teacher where id="+tutor.getId();
			ResultSet resultSet;
			try {
				resultSet = conn.createStatement().executeQuery(checktidsql);
				if (!resultSet.first()) {
					tutor.setId(0);
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				return false;
			}
			if (tutor.getId()!=0) {
				
				String sqlString = "insert into student values(?,?,?)";
				PreparedStatement pStatement;
				try {
					pStatement = conn.prepareStatement(sqlString);
					pStatement.setInt(1, student.getId());
					pStatement.setString(2, student.getName());
					pStatement.setInt(3, tutor.getId());
					if (pStatement.executeUpdate()==1) return true;
					else return false;
						
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					return false;
				}
			}
			else {
				String sqlString = "insert into student values(?,?,null)";
				PreparedStatement pStatement;
				try {
					pStatement = conn.prepareStatement(sqlString);
					pStatement.setInt(1, student.getId());
					pStatement.setString(2, student.getName());
					if (pStatement.executeUpdate()==1) return true;
					else return false;
						
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					return false;
				}
			}
		}
		else {
			String sqlString = "insert into student values(?,?,null)";
			PreparedStatement pStatement;
			try {
				pStatement = conn.prepareStatement(sqlString);
				pStatement.setInt(1, student.getId());
				pStatement.setString(2, student.getName());
				if (pStatement.executeUpdate()==1) return true;
				else return false;
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				return false;
			}
		}	
	}
	public static boolean updateStudent(Student student) {
		String sqlString = "update student set name = ? where id=?";
		Connection conn = DBUtil.getConn();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sqlString);
			pstmt.setString(1, student.getName());
			pstmt.setInt(2, student.getId());
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean deleteStudent(Student student) {
		Connection conn = DBUtil.getConn();
		String sqlString = "delete from student where id=?";
		try {
			PreparedStatement pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, student.getId());
			if (pStatement.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean chooseTutor(Student student, Teacher teacher) {
		String sqlString = "update student set tid = ? where id=?";
		Connection conn = DBUtil.getConn();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sqlString);
			pstmt.setInt(1, teacher.getId());
			pstmt.setInt(2, student.getId());
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static ArrayList<Student> showAllStudents() {
		Connection conn = DBUtil.getConn();
		String sqlString = "select * from student";
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlString);
			ArrayList<Student> students = new ArrayList<Student>();
			while (resultSet.next()) {
				Student student = new Student();
				student.setId(resultSet.getInt(1));
				student.setName(resultSet.getString(2));
				int tid = resultSet.getInt(3);
				if (tid != 0) {
					String sqlString2 = "select * from teacher where id="+tid;
					ResultSet resultSet2 = conn.createStatement().executeQuery(sqlString2);
					if (resultSet2.next()) {
						Teacher teacher = new Teacher(resultSet2.getInt(1), resultSet2.getString(2));
						student.setMytutor(teacher);
					}else {
						student.setMytutor(null);
					}
				}else {
					student.setMytutor(null);
				}
				students.add(student);
			}
			for (int i=0; i<students.size(); i++) {
				System.out.println(students.get(i).toString());
			}
			return students;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
	}

}
