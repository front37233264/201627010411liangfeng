package cn.edu.scau.cmi.liangfeng.dao.customize;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TeacherDAO {
	public static boolean addTeacher(Teacher teacher) {
		Connection conn = DBUtil.getConn();
		String sqlString = "insert into teacher values(?,?)";
		try {
			PreparedStatement pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, teacher.getId());
			pStatement.setString(2, teacher.getName());
			if (pStatement.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean updateTeacher(Teacher teacher) {
		String sqlString = "update teacher set name = ? where id=?";
		Connection conn = DBUtil.getConn();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sqlString);
			pstmt.setString(1, teacher.getName());
			pstmt.setInt(2, teacher.getId());
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean deleteTeacher(Teacher teacher) {
		Connection conn = DBUtil.getConn();
		String sqlString = "delete from teacher where id=?";
		try {
			PreparedStatement pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, teacher.getId());
			if (pStatement.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean modifyTeachingGay(Teacher teacher, Student student) {
		Connection conn = DBUtil.getConn();
		String sqlString = "update student set tid=NULL where id=?";
		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(sqlString);
			pstmt.setInt(1, student.getId());
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static ArrayList<String> showAllTeachers() {
		Connection conn = DBUtil.getConn();
		String sqlString = "select * from teacher";
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlString);
			ArrayList<String> list = new ArrayList<>();
			while (resultSet.next()) {
				list.add("teacherID:"+resultSet.getInt(1)+" name:"+resultSet.getString(2));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	public static Teacher geTeacherById(int id){
		Connection conn = DBUtil.getConn();
		String sqlString = "select * from teacher where id="+id;
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlString);
			if (resultSet.next()){
				Teacher teacher  = new Teacher(id, resultSet.getString(2));
				return teacher;
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
