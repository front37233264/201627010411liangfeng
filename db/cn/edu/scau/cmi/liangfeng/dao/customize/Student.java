package cn.edu.scau.cmi.liangfeng.dao.customize;


public class Student {
	private int id;
	private String name;
	private Teacher mytutor;
	
	public Student() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Teacher getMytutor() {
		return mytutor;
	}
	public void setMytutor(Teacher mytutor) {
		this.mytutor = mytutor;
	}
	@Override
	public String toString() {
		if (mytutor!=null)
			return "id:"+this.id+" name:"+this.name+" tutor:"+this.mytutor.getName();
		else
			return "id:"+this.id+" name:"+this.name+" tutor:null";
	}

}
