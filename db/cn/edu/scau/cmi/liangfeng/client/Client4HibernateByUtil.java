package cn.edu.scau.cmi.liangfeng.client;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.edu.scau.cmi.liangfeng.hibernate.dao.ScauCmiHibernateSessionFactoryUtil;
import cn.edu.scau.cmi.liangfeng.hibernate.domain.Student;
import cn.edu.scau.cmi.liangfeng.hibernate.domain.Teacher;


public class Client4HibernateByUtil {

	public static void main(String[] args) {
		Teacher teacher = new Teacher();
		teacher.setName("Mr3 ququ");
		newTeacher(teacher);
		listTeachers();
		Student student = new Student();
		//无需设置id，会自动根据表中的最大id自增长得到id
		student.setName("louou");
		student.setTid(3);
		newStudent(student);
		listStudents();
	}

	public static void newStudent(Student student) {
		Session session = ScauCmiHibernateSessionFactoryUtil.getSession();
		Transaction transaction = session.beginTransaction();
	
// 		(1) 直接使用Hibernate的CRUD功能		
		session.save(student);
		
		transaction.commit();
		session.close();
	}	
	public static void newTeacher(Teacher teacher) {
		Session session = ScauCmiHibernateSessionFactoryUtil.getSession();
		Transaction transaction = session.beginTransaction();
// 		(1) 直接使用Hibernate的CRUD功能		
		session.save(teacher);
		
		transaction.commit();
		session.close();
	}
	private static void listStudents() {
		Session session = ScauCmiHibernateSessionFactoryUtil.getSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("from Student");
		List<Student> list = query.list();
		for (int i=0; i<list.size(); i++)
			System.out.println(list.get(i).toString());
		
		transaction.commit();
		session.close();
	}
	
	private static void listTeachers() {
		Session session = ScauCmiHibernateSessionFactoryUtil.getSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("from Teacher");
		List<Teacher> list = query.list();
		for (int i=0; i<list.size(); i++)
			System.out.println(list.get(i).toString());
		
		transaction.commit();
		session.close();
	}

}
