package cn.edu.scau.cmi.liangfeng.client;

import java.util.ArrayList;

import cn.edu.scau.cmi.liangfeng.dao.customize.Student;
import cn.edu.scau.cmi.liangfeng.dao.customize.StudentDAO;
import cn.edu.scau.cmi.liangfeng.dao.customize.Teacher;
import cn.edu.scau.cmi.liangfeng.dao.customize.TeacherDAO;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ClientByFx extends Application{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		Pane root = new Pane();
		Scene scene = new Scene(root,600,475);
		
		VBox vBoxForStudent = new VBox();
		Label label_student = new Label("学生");
		Label label_studentid = new Label("学生id:");
		TextField tf_studentid = new TextField();
		Label label_studentname = new Label("学生姓名:");
		TextField tf_studentname = new TextField();
		Label label_tutorid = new Label("学生导师id:");
		TextField tf_tutorid = new TextField();
		Label label_allstudents = new Label("所有学生信息:");
		TextArea ta_allstudents = new TextArea();
		Button bt_addStudent = new Button("添加");
		Button bt_delStudent = new Button("删除");
		Button bt_updateStudent = new Button("修改");
		Button bt_showAllStudent = new Button("查看所有");
		HBox student_btHbox = new HBox();
		student_btHbox.setSpacing(5);
		student_btHbox.getChildren().addAll(bt_addStudent,
				bt_delStudent,bt_updateStudent,bt_showAllStudent);
		
		bt_addStudent.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Student student = new Student();
				student.setId(Integer.valueOf(tf_studentid.getText()));
				student.setName(tf_studentname.getText());
				Teacher tutor = TeacherDAO.geTeacherById(Integer.valueOf(tf_tutorid.getText()));
				if (tutor!=null){
					student.setMytutor(tutor);
				}else {
					student.setMytutor(null);
				}
				if (StudentDAO.addStudent(student)){
					Alert information = new Alert(Alert.AlertType.INFORMATION,"click the button to quit");
					information.setTitle("information"); 
					information.setHeaderText("Successfully to add new student");
					information.showAndWait();
				}else {
					Alert information = new Alert(Alert.AlertType.INFORMATION,"click the button to quit");
					information.setTitle("information"); 
					information.setHeaderText("failed to add new student, check your input");
					information.showAndWait();
				}
				
			}
		});
		
		bt_showAllStudent.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ArrayList<Student> list = StudentDAO.showAllStudents();
				for (int i=0; i<list.size(); i++){
					ta_allstudents.appendText(list.get(i).toString()+"\n\r");
				}
			}
		});
		vBoxForStudent.getChildren().addAll(
				label_studentid,tf_studentid,label_studentname,
				tf_studentname,label_tutorid,tf_tutorid,
				label_allstudents,ta_allstudents,student_btHbox);
		
		
		
		label_student.setLayoutX(14);
		label_student.setLayoutY(12);
		vBoxForStudent.setLayoutX(14);
		vBoxForStudent.setLayoutY(38);
		vBoxForStudent.setPrefSize(260, 378);
		root.getChildren().add(label_student);
		root.getChildren().add(vBoxForStudent);
		
		VBox vBoxForTeacher = new VBox();
		Label label_teacher = new Label("教师");
		Label label_teacherid = new Label("教师id:");
		TextField tf_teacherid = new TextField();
		Label label_teachername = new Label("教师姓名:");
		TextField tf_teachername = new TextField();
		Label label_teacherinform = new Label("教师介绍:");
		TextField tf_teacherinform = new TextField();
		Label label_allteachers = new Label("所有教师信息:");
		TextArea ta_allteachers = new TextArea();
		Button bt_addTeacher = new Button("添加");
		Button bt_delTeacher = new Button("删除");
		Button bt_updateTeacher = new Button("修改");
		Button bt_showAllTeacher = new Button("查看所有");
		HBox teacher_btHbox = new HBox();
		teacher_btHbox.setSpacing(5);
		teacher_btHbox.getChildren().addAll(bt_addTeacher,
				bt_delTeacher,bt_updateTeacher,bt_showAllTeacher);
		
		vBoxForTeacher.getChildren().addAll(
				label_teacherid,tf_teacherid,label_teachername,
				tf_teachername,label_teacherinform,tf_teacherinform,
				label_allteachers,ta_allteachers,teacher_btHbox);
		
		
		bt_showAllTeacher.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ArrayList<String> list = TeacherDAO.showAllTeachers();
				for (int i=0; i<list.size(); i++){
					ta_allteachers.appendText(list.get(i)+"\n\r");
				}
			}
		});
		
		label_teacher.setLayoutX(310);
		label_teacher.setLayoutY(12);
		vBoxForTeacher.setLayoutX(310);
		vBoxForTeacher.setLayoutY(38);
		vBoxForTeacher.setPrefSize(260, 378);
		root.getChildren().add(label_teacher);
		root.getChildren().add(vBoxForTeacher);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Lab7ClientByJavaFx");
		primaryStage.show();
	}

	
}
