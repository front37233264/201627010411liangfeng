package cn.edu.scau.cmi.liangfeng.client;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;
import org.hibernate.Transaction;

import cn.edu.scau.cmi.liangfeng.hibernate.dao.ScauCmiHibernateSessionFactoryUtil;
import cn.edu.scau.cmi.liangfeng.hibernate.dao.StudentDAOByHibernate;
import cn.edu.scau.cmi.liangfeng.hibernate.domain.Student;


public class Client5HibernateDao {
	
	public static void main(String[] args) {
		//BasicConfigurator.configure();
		newStudent();
		listStudents();
		System.exit(0);
	}

		public static void newStudent() {
			Session session = ScauCmiHibernateSessionFactoryUtil.getSession();
			Transaction transaction = session.beginTransaction();
			Student student=new Student();
			student.setName("小结巴");
			student.setTid(2);
//	 		(1) 直接使用Hibernate的CRUD功能		
//			session.save(student);

//			(2) 也可以将Hibernate的CRUD功能封装为DAO后，调用DAO功能。		
			StudentDAOByHibernate studentDAOByHibernate=new StudentDAOByHibernate();
			studentDAOByHibernate.save(student);
			
			transaction.commit();
			session.close();
		}

	private static void listStudents() {
		StudentDAOByHibernate studentDAO=new StudentDAOByHibernate();
		List students = studentDAO.findAll();
		Iterator<?> studentIterator = students.iterator();
		while(studentIterator.hasNext()){
			Student student = (Student) studentIterator.next();
			System.out.println("调用dao后,studentname："+student.getName()+"  studentid："+student.getId());
		}

	}

}
