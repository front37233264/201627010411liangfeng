package cn.edu.scau.cmi.liangfeng.client;

import cn.edu.scau.cmi.liangfeng.dao.customize.Student;
import cn.edu.scau.cmi.liangfeng.dao.customize.StudentDAO;
import cn.edu.scau.cmi.liangfeng.dao.customize.Teacher;
import cn.edu.scau.cmi.liangfeng.dao.customize.TeacherDAO;

public class Client3EntityDAO {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Teacher teacher = new Teacher(4, "Mrs meimei");
		
		Student student = new Student();
		student.setId(5);
		student.setName("Mike");

		StudentDAO.addStudent(student);
		TeacherDAO.addTeacher(teacher);
		
		StudentDAO.showAllStudents();
		TeacherDAO.showAllTeachers();
	}
}
