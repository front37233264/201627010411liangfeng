package cn.edu.scau.cmi.liangfeng.client;
import java.lang.management.GarbageCollectorMXBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.omg.PortableServer.ID_ASSIGNMENT_POLICY_ID;

import cn.edu.scau.cmi.liangfeng.dao.customize.DBUtil;
import cn.edu.scau.cmi.liangfeng.dao.customize.Student;
import cn.edu.scau.cmi.liangfeng.dao.customize.Teacher;


public class Client1Jdbc {
	public static void main(String[] args) {
//		System.out.println(newStudent(1, "Mary",0));
//		System.out.println(newTeacher(1, "Mrs Li"));
//		System.out.println(newStudent(2, "John",1));
//
//
//		showAllStudents();
		showAllTeachers();
	}
	public static boolean newTeacher(int id, String name) {
		Connection conn = getConn();
		String sqlString = "insert into teacher values(?,?)";
		try {
			PreparedStatement pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, id);
			pStatement.setString(2, name);
			if (pStatement.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean deleteTeacher(int id) {
		Connection conn = getConn();
		String sqlString = "delete from teacher where id=?";
		try {
			PreparedStatement pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, id);
			if (pStatement.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	public static boolean updateTeacher(int id, String newname) {
		String sqlString = "update teacher set name = ? where id=?";
		Connection conn = getConn();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sqlString);
			pstmt.setString(1, newname);
			pstmt.setInt(2, id);
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean modifyTeachingGay(int teacherid, int studentid) {
		Connection conn = getConn();
		String sqlString = "update student set tid=NULL where id=?";
		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(sqlString);
			pstmt.setInt(1, studentid);
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean newStudent(int id, String name, int tutorid) {
		Connection conn = getConn();
		String checktidsql = "select * from teacher where id="+tutorid;
		ResultSet resultSet;
		try {
			resultSet = conn.createStatement().executeQuery(checktidsql);
			if (!resultSet.first()) {
				tutorid=0;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			return false;
		}
		if (tutorid!=0) {
			
			String sqlString = "insert into student values(?,?,?)";
			PreparedStatement pStatement;
			try {
				pStatement = conn.prepareStatement(sqlString);
				pStatement.setInt(1, id);
				pStatement.setString(2, name);
				pStatement.setInt(3, tutorid);
				if (pStatement.executeUpdate()==1) return true;
				else return false;
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				return false;
			}
		}
		else {
			String sqlString = "insert into student values(?,?,null)";
			PreparedStatement pStatement;
			try {
				pStatement = conn.prepareStatement(sqlString);
				pStatement.setInt(1, id);
				pStatement.setString(2, name);
				if (pStatement.executeUpdate()==1) return true;
				else return false;
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				return false;
			}
		}
		
	}
	public static boolean newStudent(int id, String name) {
		Connection conn = getConn();
		String sqlString = "insert into student values(?,?,null)";
		PreparedStatement pStatement;
		try {
			pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, id);
			pStatement.setString(2, name);
			if (pStatement.executeUpdate()==1) return true;
			else return false;
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	public static boolean deleteStudent(int id) {
		Connection conn = getConn();
		String sqlString = "delete from student where id=?";
		try {
			PreparedStatement pStatement = conn.prepareStatement(sqlString);
			pStatement.setInt(1, id);
			if (pStatement.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	public static boolean updateStudent(int id,String newname) {
		String sqlString = "update student set name = ? where id=?";
		Connection conn = getConn();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sqlString);
			pstmt.setString(1, newname);
			pstmt.setInt(2, id);
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public static boolean studentChooseTutor(int sid, int tutorid) {
		String sqlString = "update student set tid = ? where id=?";
		Connection conn = getConn();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sqlString);
			pstmt.setInt(1, tutorid);
			pstmt.setInt(2, sid);
			if (pstmt.executeUpdate()==1) return true;
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static ArrayList<Student> showAllStudents() {
		Connection conn = getConn();
		String sqlString = "select * from student";
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlString);
			ArrayList<Student> students = new ArrayList<Student>();
			while (resultSet.next()) {
				Student student = new Student();
				student.setId(resultSet.getInt(1));
				student.setName(resultSet.getString(2));
				int tid = resultSet.getInt(3);
				if (tid != 0) {
					String sqlString2 = "select * from teacher where id="+tid;
					ResultSet resultSet2 = conn.createStatement().executeQuery(sqlString2);
					if (resultSet2.next()) {
						Teacher teacher = new Teacher(resultSet2.getInt(1), resultSet2.getString(2));
						student.setMytutor(teacher);
					}else {
						student.setMytutor(null);
					}
				}else {
					student.setMytutor(null);
				}
				students.add(student);
			}
			for (int i=0; i<students.size(); i++) {
				System.out.println(students.get(i).toString());
			}
			return students;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
	}
	public static void showAllTeachers() {
		Connection conn = DBUtil.getConn();
		String sqlString = "select * from teacher";
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlString);

			while (resultSet.next()) {
				System.out.println("teacherID:"+resultSet.getInt(1)+" name:"+resultSet.getString(2));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	private static Connection getConn() {
	    String driver = "com.mysql.jdbc.Driver";
	    String url = "jdbc:mysql://localhost:3306/test";
	    String username = "root";
	    String password = "123456";
	    Connection conn = null;
	    try {
	        Class.forName(driver); //classLoader,���ض�Ӧ����
	        conn = (Connection) DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    return conn;
	}

}