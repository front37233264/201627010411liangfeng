package cn.edu.scau.cmi.liangfeng.hibernate.dao;

import org.hibernate.Session;

public interface IBaseHibernateDAO {
	public Session getSession();
}