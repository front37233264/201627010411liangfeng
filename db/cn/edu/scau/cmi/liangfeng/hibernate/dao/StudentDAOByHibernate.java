package cn.edu.scau.cmi.liangfeng.hibernate.dao;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.scau.cmi.liangfeng.hibernate.domain.Student;

public class StudentDAOByHibernate extends BaseHibernateDAO{

	private static final Logger log = LoggerFactory.getLogger(StudentDAOByHibernate.class);
	// property constants
	public static final String NAME = "name";
	public static final String TID = "tid";
	
	public void save(Student transientInstance) {
		log.debug("saving Student instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.save(transientInstance);
			transaction.commit();
			closeSession();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Student persistentInstance) {
		log.debug("deleting Student instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.delete(persistentInstance);
			transaction.commit();
			closeSession();
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Student findById(java.lang.Long id) {
		log.debug("getting Student instance with id: " + id);
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			
			Student instance = (Student) session.get(
					"cn.edu.scau.cmi.liangfeng.hibernate.domain.Student", id);
			
			transaction.commit();
			closeSession();
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Student instance) {
		log.debug("finding Student instance by example");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			List results = session
					.createCriteria("cn.edu.scau.cmi.liangfeng.hibernate.domain.Student")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			transaction.commit();
			closeSession();
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Student instance with property: " + propertyName
				+ ", value: " + value);
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			String queryString = "from Student as model where model."
					+ propertyName + "= ?";
			Query queryObject = session.createQuery(queryString);
			queryObject.setParameter(0, value);
			transaction.commit();
			closeSession();
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findAll() {
		log.debug("finding all Student instances");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			
			String queryString = "from Student";
			Query queryObject = session.createQuery(queryString);
			transaction.commit();
			closeSession();
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Student merge(Student detachedInstance) {
		log.debug("merging Student instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			Student result = (Student)session.merge(detachedInstance);
			log.debug("merge successful");
			transaction.commit();
			closeSession();
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Student instance) {
		log.debug("attaching dirty Student instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(instance);
			transaction.commit();
			closeSession();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Student instance) {
		log.debug("attaching clean Student instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.lock(instance, LockMode.NONE);
			log.debug("attach successful");
			transaction.commit();
			closeSession();
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}
