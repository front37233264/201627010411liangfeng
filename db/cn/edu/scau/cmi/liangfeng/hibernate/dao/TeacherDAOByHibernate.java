package cn.edu.scau.cmi.liangfeng.hibernate.dao;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.scau.cmi.liangfeng.hibernate.domain.Teacher;

public class TeacherDAOByHibernate extends BaseHibernateDAO{
	private static final Logger log = LoggerFactory.getLogger(TeacherDAOByHibernate.class);
	// property constants
	public static final String NAME = "name";

	public void save(Teacher transientInstance) {
		log.debug("saving Teacher instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.save(transientInstance);
			transaction.commit();
			closeSession();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Teacher persistentInstance) {
		log.debug("deleting Teacher instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.delete(persistentInstance);
			transaction.commit();
			closeSession();
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Teacher findById(java.lang.Long id) {
		log.debug("getting Teacher instance with id: " + id);
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			Teacher instance = (Teacher)session.get(
					"cn.edu.scau.cmi.liangfeng.hibernate.domain.Teacher", id);
			transaction.commit();
			closeSession();
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Teacher instance) {
		log.debug("finding Teacher instance by example");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			List results = session
					.createCriteria("cn.edu.scau.cmi.liangfeng.hibernate.domain.Teacher")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			transaction.commit();
			closeSession();
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Teacher instance with property: " + propertyName
				+ ", value: " + value);
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			String queryString = "from Teacher as model where model."
					+ propertyName + "= ?";
			Query queryObject = session.createQuery(queryString);
			queryObject.setParameter(0, value);
			transaction.commit();
			closeSession();
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findAll() {
		log.debug("finding all Teacher instances");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			String queryString = "from Teacher";
			Query queryObject = session.createQuery(queryString);
			transaction.commit();
			closeSession();
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Teacher merge(Teacher detachedInstance) {
		log.debug("merging Teacher instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			Teacher result = (Teacher) session.merge(detachedInstance);
			transaction.commit();
			closeSession();
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Teacher instance) {
		log.debug("attaching dirty Teacher instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(instance);
			transaction.commit();
			closeSession();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Teacher instance) {
		log.debug("attaching clean Teacher instance");
		try {
			Session session = getSession();
			Transaction transaction = session.beginTransaction();
			session.lock(instance, LockMode.NONE);
			transaction.commit();
			closeSession();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

}
