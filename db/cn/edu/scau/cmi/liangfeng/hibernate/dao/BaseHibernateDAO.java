package cn.edu.scau.cmi.liangfeng.hibernate.dao;


import org.hibernate.Session;

public class BaseHibernateDAO implements IBaseHibernateDAO {
	
	public Session getSession() {
		return HibernateSessionFactoryUtil.getSession();
	}
	public void closeSession(){
		HibernateSessionFactoryUtil.closeSession();
	}
}