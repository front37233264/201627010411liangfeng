package cn.edu.scau.cmi.liangfeng.hibernate.domain;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="teacher")
public class Teacher implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(strategy="identity", name="id")
	private Integer id;
	private String name;
	public Set<Student> myStudents = new HashSet<>(0);
	
	public Teacher() {
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMyStudents(Set<Student> students){
		this.myStudents = students;
	}
	public Set<Student> getMyStudents(){
		return this.myStudents;
	}
	@Override
	public String toString(){
		return "teacherID:"+id+" name:"+name;
	}
}
