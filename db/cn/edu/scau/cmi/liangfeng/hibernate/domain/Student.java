package cn.edu.scau.cmi.liangfeng.hibernate.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="student")
public class Student implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(strategy="identity", name="id")
	private Integer id;
	private String name;
	private Teacher tutor;
	private Integer tid = 1;
	
	public Teacher getTutor() {
		return tutor;
	}
	public void setTutor(Teacher tutor) {
		this.tutor = tutor;
	}
	
	
	public Student() {
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(Integer tid) {
		this.tid = tid;
	}
	@Override
	public String toString() {
		if (this.tid!=0)
			return "id:"+this.id+" name:"+this.name+" tutor:"+this.getTid();
		else
			return "id:"+this.id+" name:"+this.name+" tutor:null";
	}
	

}
